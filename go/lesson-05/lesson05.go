package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func contains_unallowed_strings(input string) bool {
	unallowed_strings := []string{"ab", "cd", "pq", "xy"}
	for _, item := range unallowed_strings {
		if strings.Contains(input, item) {
			return true
		}
	}
	return false
}

func number_of_vowels(input string) int {
	vowels := []string{"a", "e", "i", "o", "u"}
	num_vowels := 0
	for _, vowel_item := range vowels {
		num_vowels += strings.Count(input, vowel_item)
	}
	return num_vowels
}

func has_repeating_letters(input string) bool {
	for i := 1; i < len(input); i++ {
		if input[i-1] == input[i] {
			return true
		}
	}
	return false
}

func is_nice(input string) bool {

	if contains_unallowed_strings(input) {
		return false
	}
	if number_of_vowels(input) < 3 {
		return false
	}
	if !has_repeating_letters(input) {
		return false
	}
	return true
}

func get_file_as_rows(input string) []string {

	file, _ := os.Open(input)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

// loop thru all pairs and check for the pair in the reminder of the string
func contains_pair_of_two_letters(input string) bool {
	for i := 2; i < len(input)-1; i++ {
		pair := input[i-2 : i]
		reminder := input[i:]
		if strings.Contains(reminder, pair) {
			return true
		}
	}
	return false
}

func contains_one_letter_which_repeat_with_letter_inbetween(input string) bool {
	for i := 2; i < len(input); i++ {
		if input[i-2] == input[i] {
			return true
		}
	}
	return false

}

func is_nice_improved(input string) bool {
	if !contains_pair_of_two_letters(input) {
		return false
	}
	if !contains_one_letter_which_repeat_with_letter_inbetween(input) {
		return false
	}
	return true
}

func part01() {
	/*for _, row := range []string {"ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"} {
		fmt.Printf("%s is_nice : %t\n", row, is_nice(row))
	}*/

	rows := get_file_as_rows("input.txt")
	num_nice := 0
	num_not_nice := 0
	for _, line := range rows {
		if is_nice(line) {
			num_nice += 1
		} else {
			num_not_nice += 1
		}
	}

	fmt.Printf("Part 01 : Number of nice strings : %d, number of not nice : %d\n", num_nice, num_not_nice)
}

func part02() {

	/*for _, row := range []string {"xyxy", "aabcdefgaa", "aaa"} {
		fmt.Printf("%s contains_pair_of_two_letters : %t\n", row, contains_pair_of_two_letters(row))
	}*/

	/*for _, row := range []string {"qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy"} {
		fmt.Printf("%s is_nice_improved : %t\n", row, is_nice_improved(row))
	}*/

	rows := get_file_as_rows("input.txt")
	num_nice := 0
	num_not_nice := 0
	for _, line := range rows {
		if is_nice_improved(line) {
			num_nice += 1
		} else {
			num_not_nice += 1
		}
	}

	fmt.Printf("Part 02 : Number of nice strings : %d, number of not nice : %d\n", num_nice, num_not_nice)

}

func main() {

	part01()
	part02()
}
