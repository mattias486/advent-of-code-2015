package main

import (
	"fmt"
	"io/ioutil"
)

func get_floor_number(input string) int {
	floor_number := 0
	for _, char := range input {
		if char == 40 { // (
			floor_number += 1
		}
		if char == 41 { // )
			floor_number -= 1
		}
	}
	return floor_number
}

func get_char_pos_of_first_negative_floor(input string) int {
	floor_number := 0
	for pos, char := range input {
		if char == 40 { // (
			floor_number += 1
		}
		if char == 41 { // )
			floor_number -= 1
		}
		if floor_number == -1 {
			return pos + 1 // +1 since starting with floor 1
		}
	}
	return 0
}

func main() {
	dat, _ := ioutil.ReadFile("input.txt")
	floor_number := get_floor_number(string(dat))
	fmt.Printf("Final floor : %d\n", floor_number)

	first_negative_position := get_char_pos_of_first_negative_floor(string(dat))
	fmt.Printf("Position for first time on floor -1 : %d\n", first_negative_position)
}
