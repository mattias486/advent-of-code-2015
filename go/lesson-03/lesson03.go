package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func move(x int, y int, direction rune) (int, int) {
	switch direction {
	case '^':
		return x, y - 1
	case 'v':
		return x, y + 1
	case '<':
		return x - 1, y
	case '>':
		return x + 1, y
	}
	panic("Unexpected character in input")
}

type position struct {
	x int
	y int
}

// part 01
func single_santa_visits() {
	file, _ := os.Open("input.txt")
	defer file.Close()

	reader := bufio.NewReader(file)
	var x, y int

	visits := make(map[position]int)
	visits[position{x: x, y: y}] = 1

	for {
		direction, _, err := reader.ReadRune()
		if err == io.EOF {
			break
		}
		x, y = move(x, y, direction)
		visits[position{x: x, y: y}] += 1
		//fmt.printf("position x: %d, y : %d. moved : %s\n", x, y, string(direction))
	}
	fmt.Printf("house visited atleast once by santa : %d\n", len(visits))
}

// part 02
func santa_and_robo_santa_visits() {
	file, _ := os.Open("input.txt")
	defer file.Close()

	reader := bufio.NewReader(file)
	var sx, sy, rx, ry int

	visits := make(map[position]int)
	visits[position{x: 0, y: 0}] = 2

	for i := 0; ; i++ {
		direction, _, err := reader.ReadRune()
		if err == io.EOF {
			break
		}

		if i&1 == 0 {
			sx, sy = move(sx, sy, direction)
			visits[position{x: sx, y: sy}] += 1
			//fmt.Printf("santa position x: %d, y : %d. moved : %s\n", sx, sy, string(direction))

		} else {
			rx, ry = move(rx, ry, direction)
			visits[position{x: rx, y: ry}] += 1
			//fmt.Printf("robo position x: %d, y : %d. moved : %s\n", rx, ry, string(direction))

		}
	}
	fmt.Printf("house visited atleast once by santa and robo santa : %d\n", len(visits))
}

func main() {
	single_santa_visits()
	santa_and_robo_santa_visits()
}
