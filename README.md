# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Trying out go to solve some advent of code tasks.

Main purpose being trying out [golang](https://golang.org/).

[Advent of code 2015](https://adventofcode.com/2015)

### How do I get set up? ###

* Install go
* Run the different lessons

### Notes on go development

Development done using vim and [vim-go](https://github.com/fatih/vim-go).
Using [solarized](https://github.com/altercation/solarized.git) dark theme.

Auto format go code :

gofmt -w <filename>

