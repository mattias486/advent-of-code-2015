package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func get_file_as_rows(input string) []string {

	file, _ := os.Open(input)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

type position struct {
	x int
	y int
}

func get_coord_from_string(input string) position {
	sliced := strings.Split(input, ",")
	x, _ := strconv.Atoi(sliced[0])
	y, _ := strconv.Atoi(sliced[1])
	return position{x: x, y: y}
}

func process_row_item(start position, end position, process_function func(int) int, coords *[][]int) {
	for dx := start.x; dx <= end.x; dx++ {
		for dy := start.y; dy <= end.y; dy++ {
			(*coords)[dx][dy] = process_function((*coords)[dx][dy])
		}
	}
}

func get_coordinates(row string, start_position int, end_position int) (position, position) {
	row_parts := strings.Split(row, " ")
	start := get_coord_from_string(row_parts[start_position])
	end := get_coord_from_string(row_parts[end_position])
	return start, end
}

func process_turn_off(row string, coords *[][]int) {
	start, end := get_coordinates(row, 2, 4)
	turn_off_func := func(a int) int {
		if a == 0 {
			return a
		}
		return a - 1
	}
	process_row_item(start, end, turn_off_func, coords)
}

func process_turn_on(row string, coords *[][]int) {
	start, end := get_coordinates(row, 2, 4)
	turn_on_func := func(a int) int {
		return a + 1
	}
	process_row_item(start, end, turn_on_func, coords)
}

func process_toggle(row string, coords *[][]int) {
	start, end := get_coordinates(row, 1, 3)
	toggle_func := func(a int) int {
		return a + 2
	}
	process_row_item(start, end, toggle_func, coords)
}

func check_active_brightness(coords *[][]int) int {
	var result int
	for x := 0; x < len(*coords); x++ {
		for y := 0; y < len((*coords)[0]); y++ {
			result += (*coords)[x][y]
		}
	}
	return result
}

func process_row(row string, coords *[][]int) {
	if strings.HasPrefix(row, "turn off") {
		process_turn_off(row, coords)
	}
	if strings.HasPrefix(row, "turn on") {
		process_turn_on(row, coords)
	}
	if strings.HasPrefix(row, "toggle") {
		process_toggle(row, coords)
	}
}

func main() {

	coords := make([][]int, 1000)
	for i := 0; i < 1000; i++ {
		coords[i] = make([]int, 1000)
	}

	rows := get_file_as_rows("input.txt")
	for _, row_item := range rows {
		process_row(row_item, &coords)
	}

	brightness := check_active_brightness(&coords)
	fmt.Printf("Part 02 - Active brightness : %d\n", brightness)
}
