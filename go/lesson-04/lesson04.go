package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strconv"
)

func calculate_hash_starting_with_zeros(input string, number_of_zeros int) string {
	target := fmt.Sprintf("%0"+strconv.Itoa(number_of_zeros)+"d", 0)
	for i := 0; ; i++ {
		input := input + strconv.Itoa(i)
		md5_hash := md5.Sum([]byte(input))
		md5_string := hex.EncodeToString(md5_hash[:])

		if md5_string[0:number_of_zeros] == target {
			return input
		}
	}
}

func part01(input string) {
	hash := calculate_hash_starting_with_zeros(input, 5)
	fmt.Printf("Part01 : %s\n", hash)
}

func part02(input string) {
	hash := calculate_hash_starting_with_zeros(input, 6)
	fmt.Printf("Part02 : %s\n", hash)
}

func main() {
	input := "bgvyzdsv"
	part01(input)
	part02(input)
}
