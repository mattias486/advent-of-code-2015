package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func get_smallest_area(l int, w int, h int) int {
	values := []int{l, w, h}
	sort.Ints(values)
	return values[0] * values[1]
}

func get_paper_dimensions(l int, w int, h int) int {
	smallest_dimension := get_smallest_area(l, w, h)
	paper_needed := 2*l*w + 2*w*h + 2*h*l + smallest_dimension

	//fmt.Printf("2*%d*%d + 2*%d*%d + 2*%d*%d + %d = %d\n", l, w, w, h, h, l, smallest_dimension, paper_needed)
	return paper_needed
}

func get_ribbon_tie(l int, w int, h int) int {
	return l * w * h
}

func get_smallest_perimeter(l int, w int, h int) int {
	values := []int{l, w, h}
	sort.Ints(values)
	return 2 * (values[0] + values[1])
}

func get_ribbon_dimensions(l int, w int, h int) int {
	perimeter := get_smallest_perimeter(l, w, h)
	ribbon_tie := get_ribbon_tie(l, w, h)
	return perimeter + ribbon_tie
}

func main() {
	file, _ := os.Open("input.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	total_paper := 0
	total_ribbon := 0
	for scanner.Scan() {
		split := strings.Split(scanner.Text(), "x")
		l, _ := strconv.Atoi(split[0])
		w, _ := strconv.Atoi(split[1])
		h, _ := strconv.Atoi(split[2])

		dimensions := get_paper_dimensions(l, w, h)
		total_paper += dimensions

		ribbon := get_ribbon_dimensions(l, w, h)
		total_ribbon += ribbon
	}
	fmt.Printf("Total paper needed : %d\n", total_paper)
	fmt.Printf("Total ribbon needed : %d\n", total_ribbon)
}
